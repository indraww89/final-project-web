import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.thoughtworks.selenium.webdriven.commands.GetValue

import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.URL2)
WebUI.maximizeWindow()
WebUI.verifyElementPresent(findTestObject('Demo Bank/Page_Home/Welcome to BankSystem'), GlobalVariable.timeOUT)

WebUI.click(findTestObject('Demo Bank/Page_Login/button_Accept Cookies'))

WebUI.click(findTestObject('Demo Bank/Page_Home/Button_Login'))
WebUI.verifyElementPresent(findTestObject('Demo Bank/Page_Login/h2_Log in'), GlobalVariable.timeOUT)

WebUI.setText(findTestObject('Demo Bank/Page_Login/Text_Field_input_Email'), findTestData('ID').getValue('Email', 1))
WebUI.setText(findTestObject('Demo Bank/Page_Login/Text_Field_input_Password'), findTestData('ID').getValue('password', 1))
WebUI.click(findTestObject('Demo Bank/Page_Login/button_Log in'))

WebUI.verifyElementPresent(findTestObject('Demo Bank/Page_Home_Page/h5_Two-factor authentication is disabled'), , GlobalVariable.timeOUT)
WebUI.click(findTestObject('Demo Bank/Page_Home_Page/button_Close-POPUP'))







