import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

//disable ketika running di test suite Login Transfer
//WebUI.openBrowser(GlobalVariable.URL2)
//WebUI.maximizeWindow()

if (WebUI.verifyElementNotPresent(findTestObject('Demo Bank/Page_Home_Page/a_Cards'), GlobalVariable.timeOUT, FailureHandling.OPTIONAL)) {
	WebUI.callTestCase(findTestCase('Login_Bank'), [:], FailureHandling.STOP_ON_FAILURE)
}
	
	WebUI.click(findTestObject('Object Repository/Demo Bank/Page_Home_Page/account_Final Project'))
	
	def saldo_awal = WebUI.getText(findTestObject('Object Repository/Demo Bank/Page_ABCV78503562 - BankSystem/saldo'))
	def total_trf_awal = WebUI.getText(findTestObject('Object Repository/Demo Bank/Page_ABCV78503562 - BankSystem/total transfer'))
	
	WebUI.click(findTestObject('Demo Bank/Page_Home_Page/a_Money transfers'))
	WebUI.click(findTestObject('Demo Bank/Page_Home_Page/a_New Transfer'))
	
	WebUI.verifyElementPresent(findTestObject('Demo Bank/Page_Payment/Create new payment Internal Transfer'), 0, FailureHandling.STOP_ON_FAILURE)
	WebUI.click(findTestObject('Demo Bank/Page_Payment/Create new payment Internal Transfer'))
	
	WebUI.verifyElementPresent(findTestObject('Demo Bank/Page_internal_transfer/h2_Create internal money transfer'), 0, FailureHandling.STOP_ON_FAILURE)
	WebUI.click(findTestObject('Demo Bank/Page_internal_transfer/select_Select source account'))
	WebUI.sendKeys(findTestObject('Demo Bank/Page_internal_transfer/select_Select source account'), Keys.chord(findTestData('Transfer').getValue('Nama', 1)))
	WebUI.sendKeys(findTestObject('Demo Bank/Page_internal_transfer/select_Select source account'), Keys.chord(Keys.TAB))
	
	WebUI.sendKeys(findTestObject('Demo Bank/Page_internal_transfer/input_Destination account'), Keys.chord(findTestData('Transfer').getValue('account', 2)))
	WebUI.sendKeys(findTestObject('Demo Bank/Page_internal_transfer/select_another_accounts'), Keys.chord(Keys.TAB))
	
	WebUI.sendKeys(findTestObject('Demo Bank/Page_internal_transfer/input__Amount'), Keys.chord(findTestData('Transfer').getValue('TRF', 1)))

	WebUI.setText(findTestObject('Demo Bank/Page_internal_transfer/input_(optional)_Description'), 'Data Testing')
	
	WebUI.click(findTestObject('Demo Bank/Page_internal_transfer/button_Pay'))
	
	WebUI.verifyElementText(findTestObject('Demo Bank/Page_Home_Page/p_Money transfer was successful'), 'Money transfer was successful')
	
	WebUI.click(findTestObject('Object Repository/Demo Bank/Page_Home_Page/account_Final Project'))
	
	def saldo_akhir = WebUI.getText(findTestObject('Object Repository/Demo Bank/Page_ABCV78503562 - BankSystem/saldo'))
	def total_trf_akhir = WebUI.getText(findTestObject('Object Repository/Demo Bank/Page_ABCV78503562 - BankSystem/total transfer'))
	
	WebUI.verifyNotMatch(saldo_akhir, saldo_awal, true)
	WebUI.verifyNotMatch(total_trf_akhir, total_trf_awal, true)

	
	