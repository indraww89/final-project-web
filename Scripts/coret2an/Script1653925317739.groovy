import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.URL1)
WebUI.maximizeWindow()

WebUI.verifyElementPresent(findTestObject('Demo Shop/h1_Products'), GlobalVariable.timeOUT)
def email = 'test@abcd.abcd'
if (WebUI.verifyElementPresent(findTestObject('Demo Shop/Link_Login'), GlobalVariable.timeOUT, FailureHandling.OPTIONAL)) {
	WebUI.click(findTestObject('Demo Shop/Link_Register'))
	
	WebUI.verifyElementPresent(findTestObject('Demo Shop/TEXT_Register'), GlobalVariable.timeOUT, FailureHandling.STOP_ON_FAILURE)
	WebUI.verifyElementPresent(findTestObject('Demo Shop/TEXT_Create a new account'), GlobalVariable.timeOUT, FailureHandling.STOP_ON_FAILURE)
	
	WebUI.setText(findTestObject('Demo Shop/TEXTFIELD_Email_Input.Email'), email)
	WebUI.setEncryptedText(findTestObject('Demo Shop/TEXTFIELD_Password_Input.Password'), 'fzqqY0qJjYSGUo4h/ApBAg==')
	WebUI.setEncryptedText(findTestObject('Demo Shop/TEXTFIELD_Confirm password_Input.ConfirmPassword'), 'fzqqY0qJjYSGUo4h/ApBAg==')
	
	WebUI.verifyElementNotPresent(findTestObject('Demo Shop/VERIF TEXT - Password must be at least 6 and at max 100 characters long'), GlobalVariable.timeOUT, FailureHandling.STOP_ON_FAILURE)
	WebUI.verifyElementNotPresent(findTestObject('Demo Shop/Verif TEXT - password and confirmation password do not match'), GlobalVariable.timeOUT, FailureHandling.STOP_ON_FAILURE)

}

def verif_email = 'Halo' +email+'!'
WebUI.verifyElementText(findTestObject('Demo Shop/TEXTFIELD_Email_Input.Email'), verif_email, FailureHandling.STOP_ON_FAILURE)


