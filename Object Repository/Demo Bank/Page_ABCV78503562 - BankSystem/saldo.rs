<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>saldo</name>
   <tag></tag>
   <elementGuidId>309dfab9-8cf7-4dcd-949f-26f230b5c4e9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>p.text-green.ml-3</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>/html[1]/body[1]/div[@class=&quot;container&quot;]/main[@class=&quot;py-4&quot;]/div[@class=&quot;card mb-3&quot;]/div[@class=&quot;card-body hover-parent&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-12 col-md-6 col-lg-5&quot;]/div[@class=&quot;d-flex flex-row justify-content-between&quot;]/p[@class=&quot;text-green ml-3&quot;][count(. | //*[@class = 'text-green ml-3']) = count(//*[@class = 'text-green ml-3'])]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Account details'])[1]/following::p[4]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>text-green ml-3</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>€497.00</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;container&quot;]/main[@class=&quot;py-4&quot;]/div[@class=&quot;card mb-3&quot;]/div[@class=&quot;card-body hover-parent&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-12 col-md-6 col-lg-5&quot;]/div[@class=&quot;d-flex flex-row justify-content-between&quot;]/p[@class=&quot;text-green ml-3&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Account details'])[1]/following::p[4]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Final Project'])[1]/following::p[4]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Transfer information'])[1]/preceding::p[7]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Last transfers'])[1]/preceding::p[17]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='€497.00']/parent::*</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/p[2]</value>
   </webElementXpaths>
</WebElementEntity>
